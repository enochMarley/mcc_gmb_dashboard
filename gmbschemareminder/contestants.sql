/*
Navicat MySQL Data Transfer

Source Server         : mysmsinbox
Source Server Version : 50173
Source Host           : localhost:3306
Source Database       : gmb

Target Server Type    : MYSQL
Target Server Version : 50173
File Encoding         : 65001

Date: 2017-04-10 09:18:00
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for contestants
-- ----------------------------
DROP TABLE IF EXISTS `contestants`;
CREATE TABLE `contestants` (
  `contestant_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` text NOT NULL,
  `num_of_votes` int(11) NOT NULL,
  `thumbnail` text NOT NULL,
  `height` text NOT NULL,
  `complexion` text NOT NULL,
  `contestant_num` varchar(255) NOT NULL,
  `contestant_region` text NOT NULL,
  `status` varchar(25) NOT NULL,
  `age` text,
  `video_url` text,
  `contestant_bio` text,
  PRIMARY KEY (`contestant_id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of contestants
-- ----------------------------
INSERT INTO `contestants` VALUES ('1', 'Talata', '39518', 'http://mysmsinbox.com/gmb_api/img/cont_2017/talata_evicted.jpg', '5.7ft', 'Brown', '#3132', 'Upper east', 'not_evicted', '23', 'http://mysmsinbox.com/gmb_api/cont_videos/talata.mp4', 'Talata Helen represents the Upper East Region and is 23 years of age. Her ambition is to enter law school and do pro bono cases for abused women and children. She loves photography, children, nature, and Africa. She is an ‘information junkie’ and loves to read anything. She will love to use the GMB platform to empower women and to help them become self-dependent.');
INSERT INTO `contestants` VALUES ('2', 'Baaba', '134431', 'http://mysmsinbox.com/gmb_api/img/cont_2017/baaba.jpg', '5.7ft', 'Dark Brown', '#3192', 'Central', 'not_evicted', '26', 'http://mysmsinbox.com/gmb_api/cont_videos/baaba.mp4', 'Baaba  is 26 years of age and represents the Central Region. Her ambition in life is to bring about change in the lives of the less privileged in the society especially health related change. She loves to talk as well as give listening ears to vulnerable people in the society and will, therefore, use the GMB platform to create awareness on issues on mental health to reduce the stigma associated with mental illness and the incidence of suicide.');
INSERT INTO `contestants` VALUES ('3', 'Zeinab', '140103', 'http://mysmsinbox.com/gmb_api/img/cont_2017/zainab.jpg', '5.0ft', 'Dark Brown', '#8344', 'Northern', 'not_evicted', '23', 'http://mysmsinbox.com/gmb_api/cont_videos/zeinab.mp4', 'Zeinab  represents the Northern Region. She is 23 years of age and her ambition is to unite Ghana under one umbrella to promote the beautiful culture with peace and prosperity in order to achieve developmental goals.');
INSERT INTO `contestants` VALUES ('4', 'Nana', '63224', 'http://mysmsinbox.com/gmb_api/img/cont_2017/nana.jpg', '5.5 ft', 'Brown', '#3152', 'Western', 'not_evicted', '25', 'http://mysmsinbox.com/gmb_api/cont_videos/nana.mp4', 'Nana is 25 years of age and she represents the  Western Region.  Her ambition is to become a philanthropist.');
INSERT INTO `contestants` VALUES ('5', 'Yaa', '39363', 'http://mysmsinbox.com/gmb_api/img/cont_2017/yaa_evicted.jpg', '5.7 ft', 'Dark Brown', '#2344', 'Ashanti', 'not_evicted', '25', 'http://mysmsinbox.com/gmb_api/cont_videos/yaa.mp4', 'Yaa is 25 years of age and she represents the Ashanti Region. She says her ambition in life is to inspire people to believe in themselves. ‘I’ll bring my A-game’ on, she added.');
INSERT INTO `contestants` VALUES ('6', 'Serwaa', '87039', 'http://mysmsinbox.com/gmb_api/img/cont_2017/serwaa.jpg', '6.3ft', 'Dark Brown', '#1152', 'Eastern', 'not_evicted', '23', 'http://mysmsinbox.com/gmb_api/cont_videos/serwaa.mp4', 'Serwaa Sefa- Boateng is 23 years of age and represents the Eastern Region. To make a difference and hopefully win GMB season 11 to make the Eastern region proud is her ambition.');
INSERT INTO `contestants` VALUES ('7', 'Adom', '73561', 'http://mysmsinbox.com/gmb_api/img/cont_2017/adom_evicted.jpg', '5.8 ft', 'Brown', '#8544', 'Brong Ahafo', 'not_evicted', '23', 'http://mysmsinbox.com/gmb_api/cont_videos/adom.mp4', 'Adom is 23 years of age and represents the Brong Ahafo Region. Her ambition is to empower women with advanced skills to be useful to the society and Ghana as a whole.');
INSERT INTO `contestants` VALUES ('8', 'Ayeley', '16201', 'http://mysmsinbox.com/gmb_api/img/cont_2017/ayeley.jpg', '5.6ft', 'Dark Brown', '#2341', 'Greater Accra', 'not_evicted', '23', 'http://mysmsinbox.com/gmb_api/cont_videos/ayeley.mp4', 'Ayeley is 23 years of age and represents the Greater Accra. Her ambition is to have a positive impact on society and help the less privileged in society especially the brilliant but needy.');
INSERT INTO `contestants` VALUES ('9', 'Numbu', '19802', 'http://mysmsinbox.com/gmb_api/img/cont_2017/numbu.jpg', '5.8 ft', 'Dark Brown', '#3356', 'Upper west', 'not_evicted', '26', 'http://mysmsinbox.com/gmb_api/cont_videos/numbu.mp4', 'Numbu is 26 years of age and represents the Upper West Region. Her ambition is to be a productive woman to society and the world at large.');
INSERT INTO `contestants` VALUES ('10', 'Edem', '80140', 'http://mysmsinbox.com/gmb_api/img/cont_2017/edem_evicted.jpg', '5.5 ft', 'Brown', '#0989', 'Volta', 'not_evicted', '24', 'http://mysmsinbox.com/gmb_api/cont_videos/edem.mp4', 'Edem is 24 years and she represents the Volta Region. she says, her ambition is to be an outstanding role model to the young ones in society.');

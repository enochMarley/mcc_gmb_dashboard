/*
Navicat MySQL Data Transfer

Source Server         : mysmsinbox
Source Server Version : 50173
Source Host           : localhost:3306
Source Database       : gmb

Target Server Type    : MYSQL
Target Server Version : 50173
File Encoding         : 65001

Date: 2017-04-10 09:19:03
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for track_pay
-- ----------------------------
DROP TABLE IF EXISTS `track_pay`;
CREATE TABLE `track_pay` (
  `track_id` int(11) NOT NULL AUTO_INCREMENT,
  `number` varchar(255) NOT NULL,
  `transaction_id` text NOT NULL,
  `channel` varchar(255) NOT NULL,
  `amount` text NOT NULL,
  `nominee_name` text NOT NULL,
  `when` text NOT NULL,
  PRIMARY KEY (`track_id`)
) ENGINE=MyISAM AUTO_INCREMENT=172 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of track_pay
-- ----------------------------

/*
Navicat MySQL Data Transfer

Source Server         : mysmsinbox
Source Server Version : 50173
Source Host           : localhost:3306
Source Database       : gmb

Target Server Type    : MYSQL
Target Server Version : 50173
File Encoding         : 65001

Date: 2017-04-10 09:18:31
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for gmb_pay
-- ----------------------------
DROP TABLE IF EXISTS `gmb_pay`;
CREATE TABLE `gmb_pay` (
  `pay_id` int(11) NOT NULL AUTO_INCREMENT,
  `response_code` text,
  `amt_after_charges` text,
  `transaction_id` text,
  `client_reference` text,
  `description` text,
  `external_trans_id` text,
  `amount` float DEFAULT NULL,
  `number_of_votes` int(11) DEFAULT NULL,
  `charges` float DEFAULT NULL,
  `number` varchar(255) DEFAULT NULL,
  `channel` text,
  `when` text,
  PRIMARY KEY (`pay_id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of gmb_pay
-- ----------------------------
INSERT INTO `gmb_pay` VALUES ('1', '0000', '29.25', '481df347b66b42de94a842e313b06db1', '23213', 'The Airtel Money payment has been approved and processed successfully.', 'MP180823.1508.C06646', '30', '100', '0.75', '0269185603', 'momo', '2018-08-23 15:10:30');

<?php
	include "db-config.php";
	include "../includes/cloudinary/cloudinary-config.php";
	$response = array();

	if ($_SERVER['REQUEST_METHOD'] == "POST") {

		if(is_array($_FILES)) {  
		    foreach ($_FILES['galleryImages']['name'] as $key => $value){
		        $filePath = $_FILES['galleryImages']['tmp_name'][$key];
		        $fileName = "GMB-gallery-image-".substr(md5(time()), 0, 10);

		        $uploadResult = \Cloudinary\Uploader::upload($filePath, array("folder" => "gmb_gallery/", "overwrite" => true, "public_id" => $fileName));

		       	if ($uploadResult) {
		       		$uploadPath = $uploadResult['secure_url'];

		       		$insertImageDetailsQuery = "INSERT INTO gallery(gallery_title, gallery_image) VALUES('$fileName', '$uploadPath')";
		       		$queryResult = mysqli_query($database, $insertImageDetailsQuery);
		       	}       
		    }  

		    $response['success'] = true;
        	$response["message"] = 'images uploaded successfully';

        	mysqli_close($database);

            header('Content-Type: application/json');
		    echo json_encode($response);
		        
		 } 
	}
<?php
    $serverName = '127.0.0.1';
    $databaseName = 'gmb_database';
    $databaseUser = 'root';
    $databasePassword = 'P@$$w0rd!';

    $database = mysqli_connect($serverName, $databaseUser, $databasePassword, $databaseName);

    if (!$database) {
        die('unable to connect to database');
    }
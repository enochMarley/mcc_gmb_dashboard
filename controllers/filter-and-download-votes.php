<?php  

include_once "db-config.php";

if($_SERVER['REQUEST_METHOD'] == 'POST') {
	$startDate = $_POST['startDate'];
	$endDate = $_POST['endDate'];
	$output = "";

    $response = array();
    $votesArray = array();
    $allVotesArray = array();

    //query to get the categories
    $query = "SELECT * FROM track_pay WHERE DATE(`when`) BETWEEN '$startDate' AND '$endDate' ORDER BY track_id DESC";

    $result = mysqli_query($database, $query);

    if (mysqli_num_rows($result) > 0) {
    	$output .= "<table class='table' border='1'>
    					<tr>
    						<th>Mobile number</th>
    						<th>Payment channel</th>
    						<th>Cost</th>
    						<th>Text content</th>
    						<th>Date texted</th>
    					</tr>
    	";

        while ($row = mysqli_fetch_assoc($result)) {
            $number = $row['number'];
            $channel = $row['channel'];
            $amount = $row['amount'];
            $nominee = $row['nominee_name'];
            $date = $row['when'];

            $output .= "
            	<tr>
            		<td>$number</td>
            		<td>$channel</td>
            		<td>$amount</td>
            		<td>$nominee</td>
            		<td>$date</td>
            	</tr>
            ";
        }

        $output .= "</table>";

        mysqli_close($database);

        header('Content-Type: application/xls');
        header('Content-Disposition: attachment, filename=download.xls');
	    echo $output;
    } else {
        
        mysqli_close($database);
    	echo "<script>
    		alert('no data found for date range');
    		window.history.back();
    	</script>";
    }
}

?>
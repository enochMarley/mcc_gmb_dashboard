<?php

    include_once "db-config.php";

    if($_SERVER['REQUEST_METHOD'] == 'GET') {
        $response = array();
        $statsArray = array();

        //query to get the total number contestants
        $getNumberOfContestantsQuery = "SELECT * FROM contestants";
        $getNumberOfContestantsResult = mysqli_query($database, $getNumberOfContestantsQuery);
        $getNumberOfContestants  = mysqli_num_rows($getNumberOfContestantsResult) | 0;

        // query to get the total number of evicted contestants
        $getNumberOfEvictedContestantsQuery = "SELECT * FROM contestants WHERE status ='evicted'";
        $getNumberOfEvictedContestantsResult = mysqli_query($database, $getNumberOfEvictedContestantsQuery);
        $getNumberOfEvictedContestants  = mysqli_num_rows($getNumberOfEvictedContestantsResult) | 0;

        // query to get the total number of remaining contestants
        $getNumberOfRemainingContestantsQuery = "SELECT * FROM contestants WHERE status ='not_evicted'";
        $getNumberOfRemainingContestantsResult = mysqli_query($database, $getNumberOfRemainingContestantsQuery);
        $getNumberOfRemainingContestants  = mysqli_num_rows($getNumberOfRemainingContestantsResult) | 0;

        // query to get the total number of valid contestant votes
        $getNumberOfVotesQuery = "SELECT SUM(num_of_votes) AS num_of_valid_votes FROM contestants";
        $getNumberOfVotesResult = mysqli_query($database, $getNumberOfVotesQuery);
        $row = mysqli_fetch_assoc($getNumberOfVotesResult);
        $getNumberOfValidVotes  = $row['num_of_valid_votes']| 0;

        //query to get the total number of votes
        $getTotalNumberOfVotesQuery = "SELECT SUM(`number_of_votes`) as total_vote_num FROM `gmb_pay` WHERE response_code = '0000'";
        $getTotalNumberOfVotesResult = mysqli_query($database, $getTotalNumberOfVotesQuery);
        $row0 = mysqli_fetch_assoc($getTotalNumberOfVotesResult);
        $getTotalNumberOfVotes  = $row0['total_vote_num'] | 0;

        // query to get total number of mobile number votes
        $getNumberOfMOMOVotesQuery = "SELECT SUM(`number_of_votes`) as sms_vote_num FROM `gmb_pay` WHERE `channel` = 'momo' AND response_code = '0000'";
        $getNumberOfMOMOVotesResult = mysqli_query($database, $getNumberOfMOMOVotesQuery);
        $row1 = mysqli_fetch_assoc($getNumberOfMOMOVotesResult);
        $getNumberOfMOMOVotes  = $row1['sms_vote_num'] | 0;

        // query to get total number of mobile number votes
        $getNumberOfSMSVotesQuery = "SELECT SUM(`number_of_votes`) as momo_vote_num FROM `gmb_pay` WHERE `channel` = 'sms' AND response_code = '0000'";
        $getNumberOfSMSVotesResult = mysqli_query($database, $getNumberOfSMSVotesQuery);
        $row2 = mysqli_fetch_assoc($getNumberOfSMSVotesResult);
        $getNumberOfSMSVotes  = $row2['momo_vote_num'] | 0;

        $statsArray["numberOfContestants"] = $getNumberOfContestants;
        $statsArray["numberOfEvictedContestants"] = $getNumberOfEvictedContestants;
        $statsArray["numberOfRemainingContestants"] = $getNumberOfRemainingContestants;
        $statsArray["numberOfValidVotes"] = $getNumberOfValidVotes;
        $statsArray['numberOfInvalidVotes'] = ($getTotalNumberOfVotes - $getNumberOfValidVotes) | 0;
        $statsArray["numberOfTotalVotes"] = $getTotalNumberOfVotes;
        $statsArray["numberOfSMSVotes"] = $getNumberOfSMSVotes;
        $statsArray["numberOfMOMOVotes"] = $getNumberOfMOMOVotes;

        $response['success'] = true;
        $response["message"] = 'results got';
        $response["data"] = $statsArray;

        mysqli_close($database);

        header('Content-Type: application/json');
        echo json_encode($response);
    }
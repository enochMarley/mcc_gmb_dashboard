<!DOCTYPE html>
<html lang="en">
<meta http-equiv="content-type" content="text/html;charset=UTF-8" /><!-- /Added by HTTrack -->
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <!-- Tell the browser to be responsive to screen width -->
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="author" content="">
        <link rel="shortcut icon" href="favicon.ico" type='image/x-icon'/>
        <title>Ghana's Most Beautiful</title>
        <link href="assets/css/animate.css" rel="stylesheet">
        <link href="assets/css/bootstrap-grid.min.css" rel="stylesheet">
        <link href="assets/css/bootstrap-reboot.min.css" rel="stylesheet">
        <link href="assets/css/bootstrap.min.css" rel="stylesheet">
        <link href="assets/css/toastr.min.css" rel="stylesheet">
        <link href="assets/css/linear-icons.css" rel="stylesheet">
        <link href="assets/css/styles.css" rel="stylesheet">
    </head>
    <body>
        <section class="pre-loader"></section>
